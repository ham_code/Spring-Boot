package cn.ham;

import cn.ham.entity.User;
import cn.ham.service.ShopService;
import cn.ham.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author mr.ham
 * @date 2016/11/16 20:54
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ApplicationTest {
    @Autowired
    private UserService userService;

    @Autowired
    private ShopService shopService;

    /**
     * jdbctemple test
     */
    @Test
    public void jdbctempleTest(){
        User user = new User();
        user.setName("zhangsan");
        user.setMobile("15858585784");
        userService.create(user);
    }

    @Test
    public void springDateTest(){
        /*Shop shop = new Shop();
        shop.setName("成都总店");
        shop.setAddress("春熙路");
        shopService.add(shop);*/

        /*Shop shop = shopService.getByName("成都总店");
        System.out.print(shop.getName());*/
    }
}