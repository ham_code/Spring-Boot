package cn.ham.service;

import cn.ham.entity.User;

/**
 * @author jianghan@isantai.com
 * @date 2016/11/17 10:54
 */

public interface UserService {

    /**
     * 新增
     * @param user
     */
    void create(User user);

    /**
     * 删除
     * @param id
     */
    void deleteById(Integer id);
}
