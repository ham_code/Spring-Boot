package cn.ham.service.impl;

import cn.ham.dao.UserDao;
import cn.ham.entity.User;
import cn.ham.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author jianghan@isantai.com
 * @date 2016/11/17 10:56
 */
@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private UserDao userDao;

    @Override
    public void create(User user) {
        userDao.insert(user);
    }

    @Override
    public void deleteById(Integer id) {
        userDao.delete(id);
    }
}
