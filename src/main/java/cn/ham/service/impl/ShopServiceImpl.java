package cn.ham.service.impl;

import cn.ham.dao.ShopDao;
import cn.ham.entity.Shop;
import cn.ham.service.ShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author jianghan@isantai.com
 * @date 2016/11/17 16:28
 */
@Service
public class ShopServiceImpl implements ShopService{
    @Autowired
    private ShopDao shopDao;
    @Override
    public Shop getByName(String name) {
        return shopDao.findByName(name);
    }

    @Override
    public void add(Shop shop) {
        shopDao.save(shop);
    }
}
