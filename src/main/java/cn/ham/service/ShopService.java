package cn.ham.service;

import cn.ham.entity.Shop;

/**
 * @author jianghan@isantai.com
 * @date 2016/11/17 16:26
 */
public interface ShopService {
    Shop getByName(String name);
    void add(Shop shop);
}
