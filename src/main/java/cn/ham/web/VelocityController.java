package cn.ham.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author mr.ham
 * @date 2016/11/16 21:34
 */
@Controller
public class VelocityController {

    @RequestMapping("/velocity")
    public String velocity(ModelMap map){
        map.addAttribute("host","welcome velocity!");
        return "velocity";
    }
}
