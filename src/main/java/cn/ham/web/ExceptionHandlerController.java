package cn.ham.web;

import cn.ham.exception.BusinessException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author mr.ham
 * @date 2016/11/16 22:02
 */
@Controller
public class ExceptionHandlerController {

    @RequestMapping("/default-error")
    public String hello() throws Exception {
        throw new Exception("发生错误");
    }

    @RequestMapping("/json-error")
    public String json() throws BusinessException {
        throw new BusinessException("发生错误2");
    }
}
