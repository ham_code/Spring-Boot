package cn.ham.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author mr.ham
 * @date 2016/11/16 20:58
 */
@RestController
public class RestApiController {

    @RequestMapping("/rest")
    public String rest(){
        return "welcome";
    }
}
