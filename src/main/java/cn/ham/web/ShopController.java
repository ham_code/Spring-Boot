package cn.ham.web;

import cn.ham.entity.Shop;
import cn.ham.service.ShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author jianghan@isantai.com
 * @date 2016/11/17 16:54
 */
@RestController
public class ShopController {
    @Autowired
    private ShopService shopService;

    @RequestMapping("/shop/add")
    public Object add(Shop shop){
        shopService.add(shop);
        return true;
    }
}
