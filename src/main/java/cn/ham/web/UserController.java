package cn.ham.web;

import cn.ham.entity.User;
import cn.ham.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author jianghan@isantai.com
 * @date 2016/11/17 13:28
 */
@RestController
public class UserController {
    @Autowired
    private UserService userService;

    @RequestMapping("/user/add")
    public Object add(User user){
        userService.create(user);
        return true;
    }
}
