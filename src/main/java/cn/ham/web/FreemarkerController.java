package cn.ham.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author mr.ham
 * @date 2016/11/16 21:27
 */
@Controller
public class FreemarkerController {

    @RequestMapping("/freemarker")
    public String freemarker(ModelMap map){
        map.put("host","welcome freemarker!");
        return "freemarker";
    }
}
