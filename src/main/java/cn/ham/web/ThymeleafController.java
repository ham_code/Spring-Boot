package cn.ham.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author mr.ham
 * @date 2016/11/16 21:20
 */
@Controller
public class ThymeleafController {

    @RequestMapping("/thymeleaf")
    public String thymeleaf(ModelMap map){
        map.addAttribute("host","welcome thymeleaf!");
        return "thymeleaf";
    }
}
