package cn.ham.exception;

/**
 * @author mr.ham
 * @date 2016/11/16 21:47
 */
public class BusinessException extends Exception{

    public BusinessException(String message){
        super(message);
    }
}
