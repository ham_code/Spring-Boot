package cn.ham.exception;


import java.util.Date;

/**
 * @author mr.ham
 * @date 2016/11/16 21:52
 */
public class ExceptionInfo<T> {

    public static final Integer OK = 200;
    public static final Integer ERROR = 500;

    private Integer code;
    private String msg;
    private Date time;
    private T data;
    private String url;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
