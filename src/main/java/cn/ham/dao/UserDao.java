package cn.ham.dao;

import cn.ham.entity.User;

/**
 * @author jianghan@isantai.com
 * @date 2016/11/17 10:57
 */
public interface UserDao {
    /**
     * 添加
     * @param user
     */
    void insert(User user);

    /**
     * 删除
     * @param id
     */
    void delete(Integer id);
}
