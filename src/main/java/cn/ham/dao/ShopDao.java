package cn.ham.dao;

import cn.ham.entity.Shop;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author jianghan@isantai.com
 * @date 2016/11/17 16:22
 */
public interface ShopDao extends JpaRepository<Shop,Integer>{

    Shop findByName(String name);

    Shop save(Shop shop);
}
