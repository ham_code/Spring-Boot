package cn.ham.dao.impl;

import cn.ham.dao.UserDao;
import cn.ham.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * @author jianghan@isantai.com
 * @date 2016/11/17 10:58
 */
@Repository
public class UserDaoImpl implements UserDao{

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public void insert(User user) {
        jdbcTemplate.update("insert into user(name, mobile) values(?, ?)",user.getName(),user.getMobile());
    }

    @Override
    public void delete(Integer id) {
        jdbcTemplate.update("delete from user where id = ?", id);
    }
}
